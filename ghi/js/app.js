function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${starts} - ${ends}
        </div>
    </div>
    </div>
    `;
  }
  function formatDate(date) {
    const x = new Date(date);
    const format = `${x.getMonth() + 1}/${x.getDate()}/${x.getFullYear()}`;
    return format;
  }
  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const errorAlert = document.getElementById('errorAlert'); // error alert (code works without this and without the other 2 'errorAlert.style.display' lines)
    errorAlert.style.display = 'none'; // error alert (code works without this and without the 'const errorAlert' line, or the other 'errorAlert.style.display' line)

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error(`Error getting data from API.`)
      } else {
        const data = await response.json();

        let counter = 0 // for organizing cards into proper columns

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);


          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = formatDate(details.conference.starts);
            const ends = formatDate(details.conference.ends);
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const columns = document.querySelectorAll('.col');
            const column = columns[counter] // for organizing cards into proper columns
            column.innerHTML += html; // for organizing cards into proper columns
            counter++; // for organizing cards into proper columns
            if (counter === 3){counter = 0}
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
      errorAlert.style.display = 'block'; // error alert (code works without this line, without the 'const errorAlert' line, and without the other 'errorAlert.style.display' line)
    }

  });


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//         throw new Error(`Error getting data from API.`)
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();

//           const detail = details.conference["description"]
//           const detailTag = document.querySelector('.card-text');
//           detailTag.innerHTML = detail;
//           console.log(details);

//           const imageTag = document.querySelector('.card-img-top')
//           imageTag.src = details.conference.location.picture_url;

//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//       console.error(e);
//     }

//   });
